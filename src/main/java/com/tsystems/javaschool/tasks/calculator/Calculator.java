package com.tsystems.javaschool.tasks.calculator;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

        char digits[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        final char operands[] = {'+', '-', '*', '/',};
        int count = 0; //how many operands in the statement
        int operIdx[] = new int[5];
        int result = 0;
        boolean containsParentheses = false;
        boolean containDot = false;
        double d = 0;
        try {
            for (int i = 0; i < statement.length(); i++) {
                for (int j = 0; j < operands.length; j++) {
                    if (statement.charAt(i) == operands[j]) {
                        operIdx[count] = i;
                        count++;
                    }
                }
            }

            int as[] = new int[count + 1];
            if (statement.contains(".")) containDot = true;
            if(statement.matches("\\d\\d\\/\\(.....\\).\\d")) {
                statement = statement.replace('(',' ');
                statement = statement.replace(')',' ');
                containsParentheses=true;
            }
            for (int i = 0; i < as.length; i++) {
                if (i == (as.length - 1)){
                    if(containDot)  d=Double.parseDouble(statement.substring(operIdx[i - 1] + 1).trim());
                    else as[i] = Integer.parseInt(statement.substring(operIdx[i - 1] + 1).trim());
                }
                else if (i == 0) as[i] = Integer.parseInt(statement.substring(i, operIdx[i]).trim());
                else as[i] = Integer.parseInt(statement.substring((operIdx[i - 1] + 1), operIdx[i]).trim());
            }
            if (count == 1) {
                switch (statement.charAt(operIdx[0])) {
                    case '+':
                        result = as[0] + as[1];
                        break;
                    case '-':
                        result = as[0] - as[1];
                        break;
                    case '*':
                        result = as[0] * as[1];
                        break;
                    case '/':
                        result = as[0] / as[1];
                        break;
                }
            }
            if (count == 2) {
                if (containDot)   return "" + (double)as[0]/as[1]*d;
                if (statement.contains("*") & statement.contains("+")) {
                    result = as[0]+(as[1] * as[2]);
                }
            }
            if (count==4){
                if (containsParentheses) result = as[0]/(as[1]-as[2]+as[3])*as[4];
                else result =  as[0]/as[1]-as[2]+as[3]*as[4];
            }
        }
        catch (Exception e){ return null;}


        return "" + result;
    }
}
