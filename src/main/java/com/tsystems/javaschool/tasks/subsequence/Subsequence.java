package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws IllegalArgumentException {
        // TODO: Implement the logic here
        int strtIdx = 0;
        int count = 0;
        try {
            y.equals(null);
            for (Object o : x) {
                for (int i = 0; i + strtIdx < y.size(); i++) {
                    if (o.equals(y.get(strtIdx + i))) {
                        strtIdx = i;
                        count++;
                        break;
                    }
                }
            }
        } catch (NullPointerException e) {
            throw new IllegalArgumentException();
        }
        return (count==x.size());
    }
}
