package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        // TODO : Implement your solution here
        //Checking possibility  START
        if (inputNumbers.size()>10000) throw new CannotBuildPyramidException();
        int x = inputNumbers.size();
        int r = 1; // r stands for rows
        boolean isPossible = false;

        while (r*(r+1)/2<x)  r++;
        isPossible=(r*(r+1)/2==x); // if (r*(r+1)/2==x) isPossible = true;
        int c = 2*r-1; // c stands for columns
        if (inputNumbers.contains(null))  isPossible = false;
        //Checking possibility  END

        if(!isPossible){throw new CannotBuildPyramidException();}
        else {

            //Bubble Sorting START:
            int [] inputArray = inputNumbers.stream().mapToInt( (Integer i) -> i).toArray();

            for(int i = inputArray.length-1 ; i > 0 ; i--){
                for(int j = 0 ; j < i ; j++){
                    if( inputArray[j] > inputArray[j+1] ){
                        int buf = inputArray[j];
                        inputArray[j] = inputArray[j+1];
                        inputArray[j+1] = buf;
                    }
                }
            }
            //Bubble Sorting END


            // building pyramid START
            int[][] pyramid = new int[r][c];
            int center = (c / 2);// row center
            int count = 1;
            int arrIdx = 0; //  array index

            for (int i = 0, offset = 0; i < r; i++, offset++, count++) {
                int start = center - offset;
                for (int j = 0; j < count * 2; j +=2, arrIdx++) {
                    pyramid[i][start + j] = inputArray[arrIdx];
                }
            }
            return pyramid;
        }
    }
}
